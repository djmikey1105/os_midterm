#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>

#define BUFSIZE 256

const int rchar_size = 94;

void usage(char *cmd)
{
	printf("usage: %s filename filesize[KB]\n", cmd);
	exit(-1);
}

void init_rand_char(char *array)
{
	char c;	int  i = 0;
	for (c = '!'; c <= '~'; c++) array[i++] = c;

	return ;
}

void intToStr(char *str, int num) {
	sprintf(str, "%d", num);
}

int main(int argc, char **argv)
{
	int  fd, size, i;
	int  s;
	char rchar[rchar_size];
	char filename[BUFSIZE];


	if (argc != 3) usage(argv[0]);

	for (s = 0; s < 100; s++) {
		snprintf(filename, BUFSIZE, "%s%d", argv[1], s);
		if ((fd = open(filename, O_WRONLY | O_CREAT | O_EXCL, S_IRUSR | S_IWUSR)) < 0) {
			perror("open");
			exit(-1);
		}

		size = atoi(argv[2]);
		init_rand_char(rchar);
		srand((unsigned int)time(NULL));

		for (i = 0; i < 1024 * size; i++) {
			write(fd, &rchar[rand()%(rchar_size+1)], 1);
			
		}

		close(fd);
	}
	return 0;
}
